//
//  ViewController.swift
//  PlayingCard
//
//  Created by hh on 2018/11/9.
//  Copyright © 2018 hh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var deck = PlayingCardDeck()
    
    @IBOutlet var cardViews: [PlayingCardView]!
    
    lazy var animator = UIDynamicAnimator(referenceView: view)
    
    lazy var cardBehavior = CardBehavior(in: animator)
    
    fileprivate func dealCards() {
        // Do any additional setup after loading the view, typically from a nib.
        var cards = [PlayingCard]()
        for _ in 1...((cardViews.count + 1)  / 2 ) {
            if let card = deck.draw() {
                // print("\(card)")
                cards += [card, card]
            }
        }
        for cardView in cardViews {
            cardView.isFaceUp = false
            //            let card = cards.remove(at: Int(arc4random_uniform(UInt32(cards.count))))
            let card = cards.remove(at: cards.count.arc4random)
            cardView.rank = card.rank.order!
            cardView.suit = card.suit.rawValue
            cardView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(flipCard)))
            cardBehavior.addItem(cardView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dealCards()
    }
    
    private var faceUpCardViews:[PlayingCardView] {
        return cardViews.filter { $0.isFaceUp && !$0.isHidden && $0.transform != CGAffineTransform.identity.scaledBy(x: 3.0, y: 3.0) && $0.alpha == 1 }
    }
    
    private var faceUpCardViewMatch: Bool {
        return  faceUpCardViews.count == 2 &&
            faceUpCardViews[0].rank == faceUpCardViews[1].rank &&
            faceUpCardViews[0].suit == faceUpCardViews[1].suit
    }
    private var lastChosenCardView:PlayingCardView?
    @objc func flipCard(_ recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .ended:
            if let chosenCardView = recognizer.view as? PlayingCardView, faceUpCardViews.count < 2  {
                lastChosenCardView = chosenCardView
                cardBehavior.removeItem(chosenCardView)
                UIView.transition(
                    with: chosenCardView, duration: 0.5,
                    options: [.transitionFlipFromLeft],
                    animations: {chosenCardView.isFaceUp = !chosenCardView.isFaceUp } ,
                    completion: { finish in
                        let cardsToAnimate = self.faceUpCardViews
                        
                        if self.faceUpCardViewMatch {
                            UIViewPropertyAnimator.runningPropertyAnimator(
                                withDuration: 0.6,
                                delay: 0,
                                options: [],
                                animations: {
                                    cardsToAnimate.forEach {
                                        $0.transform = CGAffineTransform.identity.scaledBy(x: 3.0, y: 3.0)
                                    }
                            },
                                completion: { _ in
                                    UIViewPropertyAnimator.runningPropertyAnimator(
                                        withDuration: 0.75,
                                        delay: 0,
                                        options: [],
                                        animations: {
                                            cardsToAnimate.forEach {
                                                $0.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
                                                $0.alpha = 0
                                            }
                                    }, completion: { _ in
                                        cardsToAnimate.forEach {
                                            $0.isHidden = true
                                            $0.alpha = 1
                                            $0.transform = .identity
                                        }
                                    })
                            })
                        } else if cardsToAnimate.count == 2 {
                            
                            if chosenCardView == self.lastChosenCardView {
                                cardsToAnimate.forEach { cardview in
                                    NSLog("flip back card:\(cardview.rank) \(cardview.suit)")
                                    UIView.transition(
                                        with:  cardview, duration: 0.5,
                                        options: [.transitionFlipFromLeft],
                                        animations: { cardview.isFaceUp = false },
                                        completion: { finish in self.cardBehavior.addItem(cardview) }
                                    )
                                }
                            }
                        } else {
                            if !chosenCardView.isFaceUp {
                                self.cardBehavior.addItem(chosenCardView)
                            }
                        }
                })
            }
        default:
            break
        }
    }
    
}


extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(-self)))
        } else {
            return 0
        }
    }
}

extension CGFloat {
    var arc4random: CGFloat {
        return self * (CGFloat(arc4random_uniform(UInt32.max))/CGFloat(UInt32.max))
    }
}
