//
//  PlayingCardDeck.swift
//  PlayingCard
//
//  Created by hh on 2018/10/30.
//  Copyright © 2018 hh. All rights reserved.
//

import Foundation
struct PlayingCardDeck {
    
    private(set) var cards = [PlayingCard]()
    
    init() {
        for suit in PlayingCard.Suit.all {
            for rank in PlayingCard.Rank.all {
                cards.append(PlayingCard(suit:suit, rank: rank))
            }
        }
    }
    

    mutating func draw() -> PlayingCard? {
        if cards.count > 0 {
            return cards[Int(arc4random_uniform(UInt32(cards.count)))]
        } else {
            return nil
        }
        
    }
    
}
