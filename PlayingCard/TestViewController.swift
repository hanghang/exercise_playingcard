//
//  TestViewController.swift
//  PlayingCard
//
//  Created by hh on 2018/10/30.
//  Copyright © 2018 hh. All rights reserved.
//

import UIKit
import Alamofire


class TestViewController: UIView {

    @IBOutlet weak var imgView: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func playcard(_ sender: UIButton) {
        
        print("to PlayingCardView")
        
    }
    
    
    @IBAction func test(_ sender: UIButton) {
        let url = "https://www.baidu.com/img/bd_logo1.png"
        Alamofire.request( url ).responseData { (response) in
           
            print(" response: \(response)")
            print(" response data: \(response.data!)")
            let image = UIImage(data: response.data!)
            self.imgView.image = image
        }

    }
    

}
