//
//  ViewController.swift
//  PlayingCard
//
//  Created by hh on 2018/10/29.
//  Copyright © 2018 hh. All rights reserved.
//

import UIKit

class PlayingCardViewController: UIViewController {

    
    var deck = PlayingCardDeck()
    
    @IBOutlet weak var playingCardView: PlayingCardView! {
        didSet {
            let swipe = UISwipeGestureRecognizer.init(target: self, action: #selector(nextCard))
            swipe.direction = [.left, .right, .up]
            playingCardView.addGestureRecognizer(swipe)
            
            let pinch = UIPinchGestureRecognizer.init(target: playingCardView, action: #selector(playingCardView.adjustFaceCardScale(byHandlingGestureRecognizedBy:)))
            playingCardView.addGestureRecognizer(pinch)
        }
    }
    
    @IBAction func flipCard(_ sender: UITapGestureRecognizer) {
        switch sender.state {
        case .ended:
            playingCardView.isFaceUp = !playingCardView.isFaceUp
        default: break
        }
    }
    
    @objc func nextCard() {
        print("swipe")
        if let card = deck.draw() {
            playingCardView.suit = card.suit.rawValue
            playingCardView.rank = card.rank.order!
        }
    }
    
    
    
}

